
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;

import com.sun.javacard.apduio.Apdu;
import com.sun.javacard.apduio.CadT1Client;
import com.sun.javacard.apduio.CadTransportException;


public class Client {
	 final static byte Wallet_CLA =(byte)0xB0;
	  // codes of INS byte in the command APDU header
	  final static byte VERIFY = (byte) 0x20;
	  final static byte CREDIT = (byte) 0x30;
	  final static byte DEBIT = (byte) 0x40;
	  final static byte GET_BALANCE = (byte) 0x50;
	  
	  static int mon;
	  public static void main(String[] args) throws IOException, CadTransportException {
		  
		  
			CadT1Client cad; 
			Socket sckClient;
			/**************** Connexion � la carte ****************/ 
			try{ 
			   sckClient=new Socket("localhost",9025); 
			   sckClient.setTcpNoDelay(true); 
			   BufferedInputStream input= new
			      BufferedInputStream(sckClient.getInputStream()); 
			   BufferedOutputStream output=new 
			      BufferedOutputStream(sckClient.getOutputStream()); 
			   cad= new CadT1Client(input,output); 
			} 
			catch(Exception e){ 
			   System.out.println("Impossible de se connecter � la carte"); 
			   return; 
			}
			/************* Mise sous tension de la carte ******************/ 
			try{ 
				   cad.powerUp(); 
				} 
				catch(Exception e){ 
				   System.out.println("Erreur lors de l'envoi de la commande powerup � la carte"); 
				   return; 
				}
			
			/********* S�lection de l'applet: Commande APDU de type SELECT *********/ 
			Apdu apdu = new Apdu(); 
			apdu.command[Apdu.CLA] = 0x00; 
			apdu.command[Apdu.INS] = (byte) 0xA4; 
			apdu.command[Apdu.P1] = 0x04; 
			apdu.command[Apdu.P2] = 0x00; 
			byte[] appletAID = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,0x07, 0x08, 0x09, 0x00, 0x00 }; 
			apdu.setDataIn(appletAID); 
			cad.exchangeApdu(apdu); 
			if (apdu.getStatus() != 0x9000) { 
			   System.out.println("Erreur lors de la s�lection de l'applet"); 
			   System.exit(1); 
			}
			 
			 
			/************************* Menu principal ***************************/
			boolean fin=false; 
			while(!fin){ 
			   System.out.println(); 
			   System.out.println("Application client JavaCard"); 
			   System.out.println("---------------------------"); 
			   System.out.println(); 
			   System.out.println("1- D�poser de l�argent sur un compte "); 
			   System.out.println("2- Retirer de l�argent depuis un compte"); 
			   System.out.println("3- consultation solde)"); 
			   
			   System.out.println("4- Quitter"); 
			   System.out.println("Votre choix :"); 
			   int choix = System.in.read(); 
			   while (!(choix >= '1' && choix<= '4')) { 
			      choix = System.in.read(); 
			}
		
			   apdu=new Apdu(); 
			   apdu.command[Apdu.CLA]=Client.Wallet_CLA; 
			   apdu.command[Apdu.P1]=0x00; 
			   apdu.command[Apdu.P2]=0x00; 
			   apdu.setLe(0x7F);
			   switch (choix) { 
			   case '1': 
				   System.out.println("Entrez votre montant qui doit ajoutez :"); 
				   mon = System.in.read(); 
				   apdu.command[Apdu.INS] = Client.CREDIT; 
				   byte[] donnees = new byte[1]; 
				   donnees[0] =(byte) mon; 
				   apdu.setDataIn(donnees); 
				   cad.exchangeApdu(apdu); 
				   if (apdu.getStatus() != 0x9000) 
				      System.out.println("Erreur : status word different de 0x9000"); 
				   else  
				      System.out.println("OK");
				   break;
				   
			   case '2': 
				   
				   System.out.println("Entrez votre montant qui doit retirer :"); 
				   mon = System.in.read(); 
				   apdu.command[Apdu.INS] = Client.CREDIT; 
				   byte[] donnee = new byte[1]; 
				   donnee[0] =(byte) mon; 
				   apdu.setDataIn(donnee); 
				   cad.exchangeApdu(apdu); 
				   if (apdu.getStatus() != 0x9000) 
				      System.out.println("Erreur : status word different de 0x9000"); 
				   else  
				      System.out.println("OK");
				   break;
				   
			   case '3': 
				   apdu.command[Apdu.INS] = Client.GET_BALANCE; 
				      cad.exchangeApdu(apdu); 
				      if (apdu.getStatus() != 0x9000)  
				         System.out.println("Erreur : status word different de 0x9000"); 
				      else 
				         System.out.println("Valeur du compteur : " +apdu.dataOut[0]);  
				   break;
			   case '4': 
				   fin=true; 
				   break;
			 
			   }
			   
			}
	  
	  
	  
	  }
	  
	  

}
