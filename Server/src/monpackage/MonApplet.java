package monpackage;

import javacard.framework.APDU;
import javacard.framework.*;
import javacard.framework.Applet;
import javacard.framework.ISOException;

public class MonApplet extends Applet {
    
    
    
    private final static byte CLA  = (byte) 0xB0 ;
    
    private final static byte INS_VERIFY_PIN  = (byte) 0x20 ;
    private final static byte INS_CREDIT  = (byte) 0x30 ;
    private final static byte INS_DEBIT  = (byte) 0x40 ;
    private final static byte INS_GET_BALANCE  = (byte) 0x50 ;
    
    private final static short MAX_BALANCE= 32767 ;
    private final static byte MAX_TRANSACTION= 127 ;
    
    private final static byte PIN_MAX_TRY=(byte) 0x03;
    private final static byte PIN_MAX_LEN=(byte) 0x04;
    
    
    OwnerPIN pin ;
    short balance=(short)0;
    
    private static short SW_PIN_VERIFICATION_FAILED = 0x6300;
    private static short SW_PIN_VERIFICATION_REQUIRED = 0x6301;
    
    private static short SW_INVALID_TRANSACTION= 0x6A83;
    private static short SW_EXCEED_BALANCE= 0x6A84;
    private static short SW_NEGATIVE_BALANCE= 0x6A85;

    
    
    private byte[] INIT_PIN = { (byte) 0, (byte) 0,(byte) 0,(byte) 0 };

    

      
    public static void install(byte[] bArray, short bOffset, byte bLength) throws ISOException {
        new MonApplet(bArray, bOffset, bLength);
    }

 
    private MonApplet(byte[] bArray, short bOffset, byte bLength) {
        pin = new OwnerPIN(PIN_MAX_TRY, PIN_MAX_LEN);
        pin.update(INIT_PIN, (short) 0 , (byte)0x04);
        register();
    }
    
    public boolean select(){
        if(pin.getTriesRemaining() == 0)
            return false ;
        return true ;
    }
    
    public void deselect (){
        pin.reset();
    }

     
    public void process(APDU apdu) throws ISOException {
        if (this.selectingApplet()){
        	return ;
        }
        byte[] buffer= apdu.getBuffer();
        if(buffer[ISO7816.OFFSET_CLA]!=CLA) 
            ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
        
        switch(buffer[ISO7816.OFFSET_INS]){

            case INS_GET_BALANCE: 
                getBalance (apdu); 
                break;
            case INS_CREDIT: 
                credit ( apdu) ; 
                break;
            case INS_DEBIT: 
                debit ( apdu) ; 
                break;
            case INS_VERIFY_PIN: 
                verify(apdu); 
                break;
            default: ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
            
                
        }
        
    }
    
    private void credit(APDU apdu){
        //if(!pin.isValidated()) ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
        byte[] buffer= apdu.getBuffer();
        byte numBytes = buffer[ISO7816.OFFSET_LC];
        byte byteRead = (byte)(apdu.setIncomingAndReceive());
        
        if( (numBytes != 1) || (byteRead!=1) ) ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        
        byte creditAmount = buffer[ISO7816.OFFSET_CDATA];
        
        if( (creditAmount>MAX_TRANSACTION) || (creditAmount <0) ) ISOException.throwIt(SW_INVALID_TRANSACTION);
        
        if ( (short) ((short)balance + (short)creditAmount )  >MAX_BALANCE ) ISOException.throwIt(SW_EXCEED_BALANCE);
        
        balance = (short) (balance + creditAmount);
    }
    
    private void debit (APDU apdu){
        //if(!pin.isValidated()) ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
        byte[] buffer= apdu.getBuffer();
        byte numBytes = buffer[ISO7816.OFFSET_LC];
        byte byteRead = (byte)(apdu.setIncomingAndReceive());
        
        if( (numBytes != 1) || (byteRead!=1) ) ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        
        byte debitAmount = buffer[ISO7816.OFFSET_CDATA];
        
        if( (debitAmount>MAX_TRANSACTION) || (debitAmount <0) ) ISOException.throwIt(SW_INVALID_TRANSACTION);
        
        if ( (short) ((short)balance - (short)debitAmount )  <  (short)0 ) ISOException.throwIt(SW_NEGATIVE_BALANCE);
        
        balance = (short) (balance - debitAmount);
        
    }
    
    private void getBalance(APDU apdu){
         //if(!pin.isValidated()) ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
        byte[] buffer= apdu.getBuffer();
        
        short le = apdu.setOutgoing();
        
        apdu.setOutgoingLength((byte)2);
        
        Util.setShort(buffer, (short)0, balance);
        
        apdu.sendBytes((short)0, (short)2);
        
    }
    
    public void verify (APDU apdu){
        byte[] buffer= apdu.getBuffer();
        byte byteRead = (byte) (apdu.setIncomingAndReceive());
        if(pin.check(buffer, ISO7816.OFFSET_CDATA, byteRead)==false)ISOException.throwIt(SW_PIN_VERIFICATION_FAILED);;
        
    }
    
    
}
